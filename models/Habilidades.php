<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "habilidades".
 *
 * @property int $id
 * @property string|null $nombre_raza
 * @property string|null $habilidades
 *
 * @property Razas $nombreRaza
 */
class Habilidades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'habilidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['nombre_raza', 'habilidades'], 'string', 'max' => 10],
            [['id'], 'unique'],
            [['nombre_raza'], 'exist', 'skipOnError' => true, 'targetClass' => Razas::className(), 'targetAttribute' => ['nombre_raza' => 'nombre_raza']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre_raza' => 'Nombre Raza',
            'habilidades' => 'Habilidades',
        ];
    }

    /**
     * Gets query for [[NombreRaza]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreRaza()
    {
        return $this->hasOne(Razas::className(), ['nombre_raza' => 'nombre_raza']);
    }
}
