<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Demonios */

$this->title = 'Update Demonios: ' . $model->cod_demonio;
$this->params['breadcrumbs'][] = ['label' => 'Demonios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_demonio, 'url' => ['view', 'id' => $model->cod_demonio]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="demonios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
